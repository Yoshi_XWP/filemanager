from flask import Flask, render_template, jsonify, request, send_file
from flask_cors import CORS
import os, errno, requests

app = Flask(__name__, static_folder = "../dist/static", template_folder = "../dist")

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
root_dir = "../uploads/"

#list all directories and files
@app.route('/api/list')
def directory_list(curr_dir = root_dir):
    list = {'d': [], 'f': []}
    files = [f for f in os.listdir(curr_dir)]
    for f in files:
        full_name = os.path.join(curr_dir, f)
        if os.path.isfile(full_name):
            list['f'].append(f)
        else:
            list['d'].append(f)

    response = {
        'files_list': list
    }
    return jsonify(response)

#create new directory, name = directory name
@app.route('/api/new')
def create_directory():
    name = request.args.get('name')
    if not name:
        response = { 'err': 'name cannot be empty' }
        return jsonify(response)
    try:
        os.mkdir(root_dir + name, 0o755)
    except OSError as exc:
        if exc.errno != errno.EEXIST: #directory already exists, just return the list
            raise
        pass
    return directory_list()

#delete existing directory or file, name = directory or file name
@app.route('/api/del')
def delete_path(curr_dir = root_dir):
    name = request.args.get('name')
    if not name:
        response = { 'err': 'name cannot be empty' }
        return jsonify(response)
    full_name = os.path.join(curr_dir, name)
    try:
        if os.path.isfile(full_name):
            os.remove(full_name)
        else:
            os.rmdir(full_name)

    except OSError as exc:
        if exc.errno != errno.ENOENT: #no such directory or file, just return the list
            raise
        pass
    return directory_list()

@app.route('/api/download')
def download_file():
    name = request.args.get('name')
    if not name:
        response = { 'err': 'file name cannot be empty' }
        return jsonify(response)
    if not os.path.exists(root_dir + name):
        response = { 'err': 'file ' + root_dir + name + ' does not exist'}
        return jsonify(response)
    normal_name = os.path.abspath(root_dir + name);
    return send_file(normal_name, None, True)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return requests.get('http://localhost:8080/{}'.format(path)).text
    #return render_template("index.html")
